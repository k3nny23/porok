package br.com.harmup.pkshop.component;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;

@Component
public class FileSaver {
	
	@Autowired
	private AmazonS3Client s3;
	
//	@Autowired
//	private HttpServletRequest request;
//	public String write(String baseFolder, MultipartFile file){
//		
//		String realPath = request.getServletContext().getRealPath("/"+baseFolder);
//		try{
//			String path = realPath +"/"+file.getOriginalFilename();
//			file.transferTo(new File(path));
//			return baseFolder+"/"+file.getOriginalFilename();
//		} catch(IOException e){
//			throw new RuntimeException(e);
//		}
//	}
	
	public String write(String baseFolder, MultipartFile multipartFile){
		try{
			s3.putObject("pkshop", multipartFile.getOriginalFilename(), multipartFile.getInputStream(), new ObjectMetadata());
			return "https://localhost:9444/s3/pkshop/"+ multipartFile.getOriginalFilename()+"?noAuth=true";
		}catch(AmazonClientException e){
			throw new RuntimeException(e);
		}catch(IOException e){
			throw new RuntimeException(e);
		}
	}
}