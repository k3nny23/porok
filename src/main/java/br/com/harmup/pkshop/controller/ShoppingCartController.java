package br.com.harmup.pkshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.harmup.pkshop.models.BookType;
import br.com.harmup.pkshop.models.Product;
import br.com.harmup.pkshop.models.ShoppingCart;
import br.com.harmup.pkshop.models.ShoppingItem;
import br.com.harmup.pkshop.repository.ProductRepository;

@Controller
@RequestMapping("/shopping")
@Scope(value=WebApplicationContext.SCOPE_REQUEST)
public class ShoppingCartController {
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private ShoppingCart shoppingCart;
	
	@RequestMapping(method=RequestMethod.POST)
	public ModelAndView add(Integer productId, BookType bookType){
		ShoppingItem item = createItem(productId, bookType);
		shoppingCart.add(item);
		ModelAndView modelAndView = new ModelAndView("redirect:/products");
		return modelAndView;
	}
	
	@RequestMapping(value="myCar",method=RequestMethod.GET)
	public ModelAndView myCar(){
		ModelAndView modelAndView = new ModelAndView("myCar");
		modelAndView.addObject("myCarQuantity", shoppingCart.getQuantity());
		return modelAndView;
	}
	
	private ShoppingItem createItem(Integer productId, BookType bookType){
		Product product = productRepository.find(productId);
		ShoppingItem item = new ShoppingItem(product, bookType);
		return item;
	}
	
}
