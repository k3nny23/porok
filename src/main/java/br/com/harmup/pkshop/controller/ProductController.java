package br.com.harmup.pkshop.controller;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.harmup.pkshop.component.FileSaver;
import br.com.harmup.pkshop.models.BookType;
import br.com.harmup.pkshop.models.Product;
import br.com.harmup.pkshop.repository.ProductRepository;

@Controller
@Transactional
@RequestMapping("/products")
public class ProductController {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private FileSaver fileSaver;

	@RequestMapping(method = RequestMethod.POST, name = "saveProduct")
	public ModelAndView save(MultipartFile summary, @Valid Product product, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) {

		if (bindingResult.hasErrors()) {
			return form(product);
		}

		String webPath = fileSaver.write("uploaded-images", summary);
		product.setSummaryPath(webPath);

		productRepository.save(product);
		redirectAttributes.addFlashAttribute("success", "Product is added with success.");
		return new ModelAndView("redirect:/products");
	}

	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public ModelAndView form(Product product) {
		ModelAndView modelAndView = new ModelAndView("product/form");
		modelAndView.addObject("types", BookType.values());
		return modelAndView;
	}
	
	@RequestMapping(value = "/show/{productId}", method= RequestMethod.GET)
	public ModelAndView show(@PathVariable("productId") Integer productId){
		ModelAndView modelAndView = new ModelAndView("product/show");
		Product product = productRepository.find(productId);
		modelAndView.addObject("product", product);
		return modelAndView;
	}

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView modelAndView = new ModelAndView("product/list");
		List<Product> products = productRepository.list();
		modelAndView.addObject("products", products);
		return modelAndView;
	}
}