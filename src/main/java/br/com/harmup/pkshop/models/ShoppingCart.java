package br.com.harmup.pkshop.models;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION)
public class ShoppingCart {
	
	private Map<ShoppingItem, Integer> items = new LinkedHashMap<ShoppingItem, Integer>();
	
	public void add(ShoppingItem shoppingItem){
		items.put(shoppingItem, getQuantity(shoppingItem)+1);
	}
	
	public Integer getQuantity(ShoppingItem shoppingItem){
		if(!items.containsKey(shoppingItem)){
			// why is it use 0 in second arg?
			items.put(shoppingItem, 0);
		}
		return items.get(shoppingItem);
	}
	
	public Integer getQuantity(){
		// WHAT ????????????
		return items.values().stream().reduce(0, (next, accumulator) -> next+accumulator);
	}
	
	public Collection<ShoppingItem> getList(){
		return items.keySet();
	}
	
	public BigDecimal getTotal(ShoppingItem shoppingItem){
		return shoppingItem.getTotal(getQuantity(shoppingItem));
	}
	
	public BigDecimal getTotal(){
		BigDecimal total = BigDecimal.ZERO;
		//TODO change to reduce?
		for(ShoppingItem item : items.keySet()){
			total = total.add(getTotal(item));
		}
		return total;
	}
	
	public void remove(ShoppingItem shoppingItem){
		items.remove(shoppingItem);
	}
	
	public boolean isEmpty(){
		return items.isEmpty();
	}
}
