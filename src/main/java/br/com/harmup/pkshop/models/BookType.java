package br.com.harmup.pkshop.models;

public enum BookType {
	EBOOK, PRINTED, COMBO
}
