package br.com.harmup.pkshop.models;

import java.math.BigDecimal;

public class ShoppingItem {

	private BookType bookType;
	private Product product;
	
	public ShoppingItem(Product product, BookType bookType){
		this.product = product;
		this.bookType = bookType;
	}
	
	public BookType getBookType(){
		return this.bookType;
	}
	
	public Product getProduct(){
		return this.product;
	}
	
	public BigDecimal getPrice(){
		return product.priceFor(bookType);
	}
	
	public BigDecimal getTotal(Integer quantity){
		return getPrice().multiply(new BigDecimal(quantity));
	}
}
