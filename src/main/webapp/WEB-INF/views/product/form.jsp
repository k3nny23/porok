<%@ taglib prefix="c" 		uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"	uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" 	uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Register of Product</title>
</head>
<body>
	<form:form action="${spring:mvcUrl('saveProduct').build()}" method="post" commandName="product" enctype="multipart/form-data">
		<div>
			<label for="title">Title</label>
			<form:input path="title"/>
			<form:errors path="title"/>
		</div>
		<div>
			<label for="description">Description</label>
			<form:textarea path="description" rows="10" cols="20"/>
			<form:errors path="description"/>
		</div>
		<c:forEach items="${types}" var="bookType" varStatus="status">
			<div>
				<label for="price_${bookType}">${bookType}</label>
				<input type="text" name="prices[${status.index}].value" id="price_${bookType }"/>
				<input type="hidden" name="prices[${status.index}].bookType" value="${bookType }"/>
			</div>
		</c:forEach>
		<div>
			<label for="releaseDate">Release date</label>
			<form:input path="releaseDate" type="date"/>
			<form:errors path="releaseDate"/>
		</div>
		<div>
			<label for="pages">Page Numbers</label>
			<input type="text" name="pages" id="pages"/>
			<form:errors path="pages"/>
		</div>
		<div>
			<label for="summary">Summary of book</label>
			<input type="file" name="summary"/>
			<form:errors path="summaryPath"/>
		</div>
		<div>
			<input type="submit" value="Register"/>
		</div>
	</form:form>
</body>
</html>